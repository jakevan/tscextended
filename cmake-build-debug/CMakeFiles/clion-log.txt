C:\Users\Jake\AppData\Local\JetBrains\Toolbox\apps\CLion\ch-0\212.3116.34\bin\cmake\win\bin\cmake.exe -DCMAKE_BUILD_TYPE=Debug -DCMAKE_DEPENDS_USE_COMPILER=FALSE -G "CodeBlocks - MinGW Makefiles" C:\Users\Jake\CLionProjects\tscextended
-- The C compiler identification is GNU 8.1.0
-- The CXX compiler identification is GNU 8.1.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: C:/Program Files/mingw-w64/x86_64-8.1.0-posix-seh-rt_v6-rev0/mingw64/bin/gcc.exe - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: C:/Program Files/mingw-w64/x86_64-8.1.0-posix-seh-rt_v6-rev0/mingw64/bin/g++.exe - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
CMake Warning at CMakeLists.txt:517 (find_package):
  By not providing "FindSDL2.cmake" in CMAKE_MODULE_PATH this project has
  asked CMake to find a package configuration file provided by "SDL2", but
  CMake did not find one.

  Could not find a package configuration file provided by "SDL2" with any of
  the following names:

    SDL2Config.cmake
    sdl2-config.cmake

  Add the installation prefix of "SDL2" to CMAKE_PREFIX_PATH or set
  "SDL2_DIR" to a directory containing one of the above files.  If "SDL2"
  provides a separate development package or SDK, be sure it has been
  installed.


-- Using local SDL2
CMake Deprecation Warning at external/SDL2/CMakeLists.txt:5 (cmake_minimum_required):
  Compatibility with CMake < 2.8.12 will be removed from a future version of
  CMake.

  Update the VERSION argument <min> value or use a ...<max> suffix to tell
  CMake that the project does not need compatibility with older versions.


-- Could NOT find PkgConfig (missing: PKG_CONFIG_EXECUTABLE) 
-- 
-- SDL2 was configured with the following options:
-- 
-- Platform: Windows-10.0.19041
-- 64-bit:   TRUE
-- Compiler: C:/Program Files/mingw-w64/x86_64-8.1.0-posix-seh-rt_v6-rev0/mingw64/bin/gcc.exe
-- 
-- Subsystems:
--   Atomic:	ON
--   Audio:	ON
--   Video:	ON
--   Render:	ON
--   Events:	ON
--   Joystick:	ON
--   Haptic:	ON
--   Power:	ON
--   Threads:	ON
--   Timers:	ON
--   File:	ON
--   Loadso:	ON
--   CPUinfo:	ON
--   Filesystem:	ON
--   Dlopen:	ON
--   Sensor:	ON
-- 
-- Options:
--   3DNOW                  (Wanted: ON): ON
--   ALSA                   (Wanted: OFF): OFF
--   ALSA_SHARED            (Wanted: OFF): OFF
--   ALTIVEC                (Wanted: ON): OFF
--   ARMNEON                (Wanted: ON): OFF
--   ARMSIMD                (Wanted: ON): OFF
--   ARTS                   (Wanted: OFF): OFF
--   ARTS_SHARED            (Wanted: OFF): OFF
--   ASSEMBLY               (Wanted: ON): ON
--   ASSERTIONS             (Wanted: auto): auto
--   BACKGROUNDING_SIGNAL   (Wanted: OFF): OFF
--   CLOCK_GETTIME          (Wanted: OFF): OFF
--   DIRECTFB_SHARED        (Wanted: OFF): OFF
--   DIRECTX                (Wanted: ON): ON
--   DISKAUDIO              (Wanted: ON): ON
--   DUMMYAUDIO             (Wanted: ON): ON
--   ESD                    (Wanted: OFF): OFF
--   ESD_SHARED             (Wanted: OFF): OFF
--   FOREGROUNDING_SIGNAL   (Wanted: OFF): OFF
--   FUSIONSOUND            (Wanted: OFF): OFF
--   FUSIONSOUND_SHARED     (Wanted: OFF): OFF
--   GCC_ATOMICS            (Wanted: ON): ON
--   HIDAPI                 (Wanted: ON): ON
--   INPUT_TSLIB            (Wanted: OFF): OFF
--   JACK                   (Wanted: OFF): OFF
--   JACK_SHARED            (Wanted: OFF): OFF
--   KMSDRM_SHARED          (Wanted: OFF): OFF
--   LIBC                   (Wanted: ON): ON
--   LIBSAMPLERATE          (Wanted: OFF): OFF
--   LIBSAMPLERATE_SHARED   (Wanted: OFF): OFF
--   MMX                    (Wanted: ON): ON
--   NAS                    (Wanted: OFF): OFF
--   NAS_SHARED             (Wanted: OFF): OFF
--   OSS                    (Wanted: OFF): OFF
--   PTHREADS               (Wanted: OFF): OFF
--   PTHREADS_SEM           (Wanted: OFF): OFF
--   PULSEAUDIO             (Wanted: OFF): OFF
--   PULSEAUDIO_SHARED      (Wanted: OFF): OFF
--   RENDER_D3D             (Wanted: ON): ON
--   RENDER_METAL           (Wanted: OFF): OFF
--   RPATH                  (Wanted: OFF): OFF
--   SDL_DLOPEN             (Wanted: ON): OFF
--   SDL_STATIC_PIC         (Wanted: OFF): OFF
--   SDL_TEST               (Wanted: OFF): OFF
--   SNDIO                  (Wanted: OFF): OFF
--   SSE                    (Wanted: ON): ON
--   SSE2                   (Wanted: ON): ON
--   SSE3                   (Wanted: ON): ON
--   SSEMATH                (Wanted: ON): OFF
--   VIDEO_COCOA            (Wanted: OFF): OFF
--   VIDEO_DIRECTFB         (Wanted: OFF): OFF
--   VIDEO_DUMMY            (Wanted: ON): ON
--   VIDEO_KMSDRM           (Wanted: OFF): OFF
--   VIDEO_METAL            (Wanted: OFF): OFF
--   VIDEO_OFFSCREEN        (Wanted: OFF): OFF
--   VIDEO_OPENGL           (Wanted: ON): ON
--   VIDEO_OPENGLES         (Wanted: ON): ON
--   VIDEO_RPI              (Wanted: OFF): OFF
--   VIDEO_VIVANTE          (Wanted: OFF): OFF
--   VIDEO_VULKAN           (Wanted: ON): ON
--   VIDEO_WAYLAND          (Wanted: OFF): OFF
--   VIDEO_WAYLAND_QT_TOUCH (Wanted: OFF): OFF
--   VIDEO_X11              (Wanted: OFF): OFF
--   VIDEO_X11_XCURSOR      (Wanted: OFF): OFF
--   VIDEO_X11_XINERAMA     (Wanted: OFF): OFF
--   VIDEO_X11_XINPUT       (Wanted: OFF): OFF
--   VIDEO_X11_XRANDR       (Wanted: OFF): OFF
--   VIDEO_X11_XSCRNSAVER   (Wanted: OFF): OFF
--   VIDEO_X11_XSHAPE       (Wanted: OFF): OFF
--   VIDEO_X11_XVM          (Wanted: OFF): OFF
--   WASAPI                 (Wanted: ON): OFF
--   WAYLAND_SHARED         (Wanted: OFF): OFF
--   X11_SHARED             (Wanted: OFF): OFF
-- 
--  CFLAGS:         -idirafter "C:/Users/Jake/CLionProjects/tscextended/external/SDL2/src/video/khronos"  "-IC:/Users/Jake/CLionProjects/tscextended/external/SDL2/src/hidapi/hidapi"
--  EXTRA_CFLAGS:  -msse3 -msse2 -msse -m3dnow -mmmx -Wshadow -fvisibility=hidden -Wdeclaration-after-statement -Werror=declaration-after-statement -fno-strict-aliasing -Wall 
--  EXTRA_LDFLAGS: -Wl,--no-undefined;-mwindows
--  EXTRA_LIBS:    m;user32;gdi32;winmm;imm32;ole32;oleaut32;version;uuid;advapi32;setupapi;shell32;dinput8;dxerr8;mingw32
-- 
--  Build Shared Library: OFF
--  Build Static Library: ON
--  Build Static Library with Position Independent Code: OFF
-- 
-- Could NOT find Freetype (missing: FREETYPE_LIBRARY FREETYPE_INCLUDE_DIRS) 
-- Using local FreeType
CMake Warning (dev) at C:/Users/Jake/AppData/Local/JetBrains/Toolbox/apps/CLion/ch-0/212.3116.34/bin/cmake/win/share/cmake-3.20/Modules/FindPackageHandleStandardArgs.cmake:438 (message):
  The package name passed to `find_package_handle_standard_args` (PkgConfig)
  does not match the name of the calling package (HarfBuzz).  This can lead
  to problems in calling code that expects `find_package` result variables
  (e.g., `_FOUND`) to follow a certain pattern.
Call Stack (most recent call first):
  C:/Users/Jake/AppData/Local/JetBrains/Toolbox/apps/CLion/ch-0/212.3116.34/bin/cmake/win/share/cmake-3.20/Modules/FindPkgConfig.cmake:70 (find_package_handle_standard_args)
  external/freetype/builds/cmake/FindHarfBuzz.cmake:35 (include)
  external/freetype/CMakeLists.txt:212 (find_package)
This warning is for project developers.  Use -Wno-dev to suppress it.

-- Could NOT find PkgConfig (missing: PKG_CONFIG_EXECUTABLE) 
CMake Warning (dev) at C:/Users/Jake/AppData/Local/JetBrains/Toolbox/apps/CLion/ch-0/212.3116.34/bin/cmake/win/share/cmake-3.20/Modules/FindPackageHandleStandardArgs.cmake:438 (message):
  The package name passed to `find_package_handle_standard_args` (harfbuzz)
  does not match the name of the calling package (HarfBuzz).  This can lead
  to problems in calling code that expects `find_package` result variables
  (e.g., `_FOUND`) to follow a certain pattern.
Call Stack (most recent call first):
  external/freetype/builds/cmake/FindHarfBuzz.cmake:67 (find_package_handle_standard_args)
  external/freetype/CMakeLists.txt:212 (find_package)
This warning is for project developers.  Use -Wno-dev to suppress it.

-- Could NOT find harfbuzz (missing: HARFBUZZ_INCLUDE_DIRS HARFBUZZ_LIBRARIES) 
-- Could NOT find PNG (missing: PNG_LIBRARY PNG_PNG_INCLUDE_DIR) 
-- Could NOT find BZip2 (missing: BZIP2_LIBRARIES BZIP2_INCLUDE_DIR) 
CMake Warning (dev) at C:/Users/Jake/AppData/Local/JetBrains/Toolbox/apps/CLion/ch-0/212.3116.34/bin/cmake/win/share/cmake-3.20/Modules/FindPackageHandleStandardArgs.cmake:438 (message):
  The package name passed to `find_package_handle_standard_args` (PkgConfig)
  does not match the name of the calling package (BrotliDec).  This can lead
  to problems in calling code that expects `find_package` result variables
  (e.g., `_FOUND`) to follow a certain pattern.
Call Stack (most recent call first):
  C:/Users/Jake/AppData/Local/JetBrains/Toolbox/apps/CLion/ch-0/212.3116.34/bin/cmake/win/share/cmake-3.20/Modules/FindPkgConfig.cmake:70 (find_package_handle_standard_args)
  external/freetype/builds/cmake/FindBrotliDec.cmake:22 (include)
  external/freetype/CMakeLists.txt:236 (find_package)
This warning is for project developers.  Use -Wno-dev to suppress it.

-- Could NOT find PkgConfig (missing: PKG_CONFIG_EXECUTABLE) 
CMake Warning (dev) at C:/Users/Jake/AppData/Local/JetBrains/Toolbox/apps/CLion/ch-0/212.3116.34/bin/cmake/win/share/cmake-3.20/Modules/FindPackageHandleStandardArgs.cmake:438 (message):
  The package name passed to `find_package_handle_standard_args` (brotlidec)
  does not match the name of the calling package (BrotliDec).  This can lead
  to problems in calling code that expects `find_package` result variables
  (e.g., `_FOUND`) to follow a certain pattern.
Call Stack (most recent call first):
  external/freetype/builds/cmake/FindBrotliDec.cmake:43 (find_package_handle_standard_args)
  external/freetype/CMakeLists.txt:236 (find_package)
This warning is for project developers.  Use -Wno-dev to suppress it.

-- Could NOT find brotlidec (missing: BROTLIDEC_INCLUDE_DIRS BROTLIDEC_LIBRARIES) 
-- Configuring done
-- Generating done
-- Build files have been written to: C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug
