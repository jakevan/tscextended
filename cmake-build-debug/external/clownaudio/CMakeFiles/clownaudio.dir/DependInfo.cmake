
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/Jake/CLionProjects/tscextended/external/clownaudio/src/decoding/decoder_selector.cpp" "C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug/external/clownaudio/CMakeFiles/clownaudio.dir/src/decoding/decoder_selector.cpp.obj"
  "C:/Users/Jake/CLionProjects/tscextended/external/clownaudio/src/decoding/decoders/memory_stream.cpp" "C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug/external/clownaudio/CMakeFiles/clownaudio.dir/src/decoding/decoders/memory_stream.cpp.obj"
  "C:/Users/Jake/CLionProjects/tscextended/external/clownaudio/src/decoding/decoders/stb_vorbis.cpp" "C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug/external/clownaudio/CMakeFiles/clownaudio.dir/src/decoding/decoders/stb_vorbis.cpp.obj"
  "C:/Users/Jake/CLionProjects/tscextended/external/clownaudio/src/decoding/predecoder.cpp" "C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug/external/clownaudio/CMakeFiles/clownaudio.dir/src/decoding/predecoder.cpp.obj"
  "C:/Users/Jake/CLionProjects/tscextended/external/clownaudio/src/decoding/resampled_decoder.cpp" "C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug/external/clownaudio/CMakeFiles/clownaudio.dir/src/decoding/resampled_decoder.cpp.obj"
  "C:/Users/Jake/CLionProjects/tscextended/external/clownaudio/src/decoding/split_decoder.cpp" "C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug/external/clownaudio/CMakeFiles/clownaudio.dir/src/decoding/split_decoder.cpp.obj"
  "C:/Users/Jake/CLionProjects/tscextended/external/clownaudio/src/miniaudio.cpp" "C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug/external/clownaudio/CMakeFiles/clownaudio.dir/src/miniaudio.cpp.obj"
  "C:/Users/Jake/CLionProjects/tscextended/external/clownaudio/src/mixer.cpp" "C:/Users/Jake/CLionProjects/tscextended/cmake-build-debug/external/clownaudio/CMakeFiles/clownaudio.dir/src/mixer.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_STB_VORBIS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../external/clownaudio/include"
  "export"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
